//
//  ContactInfo.m
//  Contacts
//
//  Created by Jordan Hipwell on 3/18/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ContactInfo.h"


@implementation ContactInfo

@dynamic firstName;
@dynamic lastName;
@dynamic email;
@dynamic address;
@dynamic phone;

@end
