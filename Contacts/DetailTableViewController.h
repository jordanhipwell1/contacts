//
//  DetailViewController.h
//  Contacts
//
//  Created by Jordan Hipwell on 3/17/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactInfo.h"

@interface DetailTableViewController : UITableViewController

@property (strong, nonatomic) ContactInfo *contactInfo;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;

@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapButtonWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapButtonTrailingConstraint;

@end

