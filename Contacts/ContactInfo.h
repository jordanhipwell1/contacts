//
//  ContactInfo.h
//  Contacts
//
//  Created by Jordan Hipwell on 3/18/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ContactInfo : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * phone;

@end
