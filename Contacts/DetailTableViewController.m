//
//  DetailViewController.m
//  Contacts
//
//  Created by Jordan Hipwell on 3/17/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "DetailTableViewController.h"
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface DetailTableViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self updateView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.addressTextView addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.addressTextView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getters and Setters

- (void)setDetailItem:(id)newDetailItem {
    if (_contactInfo != newDetailItem) {
        _contactInfo = newDetailItem;
        
        [self updateView];
    }
}

#pragma mark - Methods

- (void)updateView {
    if (self.contactInfo.firstName) {
        //existing contact
        self.firstNameTextField.text = self.contactInfo.firstName;
        self.lastNameTextField.text = self.contactInfo.lastName;
        if (![self.contactInfo.phone isEqualToString:@""]) {
            self.phoneTextField.text = self.contactInfo.phone;
        } else {
            self.phoneButtonWidthConstraint.constant = 0;
            self.phoneButtonTrailingConstraint.constant = 0;
        }
        if (![self.contactInfo.email isEqualToString:@""]) {
            self.emailTextField.text = self.contactInfo.email;
        } else {
            self.emailButtonWidthConstraint.constant = 0;
            self.emailButtonTrailingConstraint.constant = 0;
        }
        if (![self.contactInfo.address isEqualToString:@""]) {
            self.addressTextView.text = self.contactInfo.address;
        } else {
            self.mapButtonWidthConstraint.constant = 0;
            self.mapButtonTrailingConstraint.constant = 0;
        }
        
        self.title = [NSString stringWithFormat:@"%@ %@", self.contactInfo.firstName, self.contactInfo.lastName];
        
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didTapSave:)];
        self.navigationItem.rightBarButtonItem = saveButton;
    } else {
        //new contact
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didTapCancel:)];
        self.navigationItem.leftBarButtonItem = cancelButton;
        
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didTapSave:)];
        self.navigationItem.rightBarButtonItem = saveButton;
        
        self.title = @"Add New Contact";
        
        self.phoneButtonWidthConstraint.constant = 0;
        self.phoneButtonTrailingConstraint.constant = 0;
        self.emailButtonWidthConstraint.constant = 0;
        self.emailButtonTrailingConstraint.constant = 0;
        self.mapButtonWidthConstraint.constant = 0;
        self.mapButtonTrailingConstraint.constant = 0;
    }
}

- (void)didTapCancel:(id)sender {
    [self performSegueWithIdentifier:@"unwindFromDetailToCancel" sender:self];
}

- (void)didTapSave:(id)sender {
    self.contactInfo.firstName = self.firstNameTextField.text;
    self.contactInfo.lastName = self.lastNameTextField.text;
    self.contactInfo.phone = self.phoneTextField.text;
    self.contactInfo.email = self.emailTextField.text;
    self.contactInfo.address = self.addressTextView.text;
    
    [self performSegueWithIdentifier:@"unwindFromDetailToSave" sender:self];
}

- (IBAction)didTapPhone:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"telprompt://" stringByAppendingString:self.phoneTextField.text]]];
}

- (IBAction)didTapEmail:(UIButton *)sender {
    NSArray *toRecipents = @[self.emailTextField.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (IBAction)didTapMap:(UIButton *)sender {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:self.addressTextView.text
                 completionHandler:^(NSArray *placemarks, NSError *error) {
                     
                     // Convert the CLPlacemark to an MKPlacemark
                     CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                     MKPlacemark *placemark = [[MKPlacemark alloc]
                                               initWithCoordinate:geocodedPlacemark.location.coordinate
                                               addressDictionary:geocodedPlacemark.addressDictionary];
                     
                     // Create a map item for the geocoded address to pass to Maps app
                     MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                     [mapItem setName: [NSString stringWithFormat:@"%@ %@", self.firstNameTextField.text, self.lastNameTextField.text]]; //geocodedPlacemark.name
                     
                     // Set the various map options mode
                     NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
                     
                     // Get the "Current User Location" MKMapItem
                     MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
                     
                     // Pass the current location and destination map items to the Maps app
                     [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
                 }];
}

//vertically center text in UITextView
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *txtview = object;
    CGFloat topoffset = ([txtview bounds].size.height - [txtview contentSize].height * [txtview zoomScale])/2.0;
    topoffset = ( topoffset < 0.0 ? 0.0 : topoffset );
    txtview.contentOffset = (CGPoint){.x = 0, .y = -topoffset};
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
